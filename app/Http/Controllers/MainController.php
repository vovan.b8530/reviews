<?php

namespace App\Http\Controllers;


use Illuminate\View\View;

class MainController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return View
     */
    public function __invoke(): View
    {
        return view('main');
    }
}
