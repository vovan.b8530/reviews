<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewRequest;
use App\Http\Resources\ReviewResource;
use App\Services\ReviewService;
use Auth;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ReviewController extends Controller
{

    protected $service;

    /**
     * @param  ReviewService  $service
     */
    public function __construct(ReviewService $service)
    {
        $this->service = $service;
        $this->middleware('auth');
    }


    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $authUser = Auth::user();

        return  ReviewResource::collection($authUser
            ->reviews()
            ->with('images', 'user')
            ->get());
    }

    /**
     * @param  ReviewRequest  $request
     * @return ReviewResource
     */
    public function store(ReviewRequest $request): ReviewResource
    {
        $data = $request->validated();
        $review = $this->service->store($data);

        return new ReviewResource($review->load('images', 'user'));
    }
}
