<?php

namespace App\Http\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/** @mixin \App\Models\Review */
class ReviewResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'text' => $this->text,

            'created_at' => Carbon::parse($this->created_at)->format('Y-m-d H:i:s'),

            'user' => new UserResource($this->whenLoaded('user')),
            'images' => ImageResource::collection($this->whenLoaded('images')),
        ];
    }
}
