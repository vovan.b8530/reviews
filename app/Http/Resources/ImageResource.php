<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Image */
class ImageResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'file_name' => $this->file_name,
            'url' => url('storage/'.$this->path),
            'url_thumb' => url('storage/'.str_replace('reviews/', 'reviews/thumb_', $this->path)),
        ];
    }
}
