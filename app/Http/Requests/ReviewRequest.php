<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{


    public function rules(): array
    {
        return [
            'text' => ['required', 'string', 'min:5', 'max:1000'],
            'images' => ['nullable', 'array'],
            'images.*' => ['nullable', 'file', 'mimes:jpg,jpeg,png', 'max:2048'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
