<?php

namespace App\Services;

use App\Models\Image;
use App\Models\Review;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as ImageManager;

class ReviewService
{
    /**
     * @param  array  $data
     * @return Review
     */
    public function store(array $data): Review
    {
        try {
            DB::beginTransaction();

            $images = $data['images'] ?? [];
            unset($data['images']);

            $data['user_id'] = Auth::id();

            $review = Review::create($data);
            $review->save();

            if ($images) {
                foreach ($images as $image) {
                    $name = md5(Carbon::now().'_'.$image->getClientOriginalName()).'.'.$image->getClientOriginalExtension();
                    $thumbName = 'thumb_'.$name;

                    $imagePath = Storage::disk('public')->putFileAs('reviews', $image, $name);

                    $imageManager = ImageManager::make($image->getRealPath());
                    $imageManager->resize(150, 150)->save(storage_path('app/public/reviews/'.$thumbName));

                    Image::create([
                        'file_name' => $name,
                        'path' => $imagePath,
                        'review_id' => $review->id,
                    ]);
                }
            }
            DB::commit();

            return $review;

        } catch (\Exception $exception) {
            DB::rollBack();
            abort(500, $exception->getMessage());
        }

        return new Review();
    }
}