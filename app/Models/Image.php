<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * Class Image
 * @package App\Models
 *
 * @property int $id
 * @property int $review_id
 * @property string $file_name
 * @property string $path
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class Image extends Model
{
    protected $fillable = [
        'review_id',
        'file_name',
        'path',
    ];

    /**
     * @return BelongsTo
     */
    public function review(): BelongsTo
    {
        return $this->belongsTo(Review::class);
    }
}
