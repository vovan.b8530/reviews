<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class Review
 * @package App\Models
 *
 * @property int $id
 * @property string $text
 * @property integer $user_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 */
class Review extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'text',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }
}
