<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Models\Review;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/main', 'MainController')->name('main');


Route::group(['prefix' => 'reviews'], function () {
    Route::get('/','ReviewController@index')->name('reviews.index');
    Route::post('/','ReviewController@store')->name('reviews.store');
});

